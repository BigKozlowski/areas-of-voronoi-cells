function voronoi_areas(points) {
  const areas = [];

  for (let i = 0; i < points.length; i++) {
    const secants = [];
    let vertexes = [];

    //create all secants for current site
    for (let j = 0; j < points.length; j++) {
      if (i !== j) {
        const p1 = findMidpoint(points[i], points[j]);
        const slope = findSecantSlope(points[i], points[j]);
        const p2 = findSecondPoint(p1, slope);
        secants.push({ p1, p2, slope });
      }
    }

    //find all secant intersections
    for (let s1 = 0; s1 < secants.length - 1; s1++) {
      for (let s2 = s1 + 1; s2 < secants.length; s2++) {
        const intersectionPoint = findIntersectionPoint(secants[s1], secants[s2]);
        if (
          intersectionPoint &&
          !vertexes.find(
            (el) => epsEqu(el.x, intersectionPoint.x) && epsEqu(el.y, intersectionPoint.y)
          )
        ) {
          vertexes.push(intersectionPoint);
        }
      }
    }

    // check intersections for being vertex of current cell and remove other
    for (let secant of secants) {
      vertexes = vertexes.filter((vertex, j) => {
        const sameHalfplane = checkHalfplane(secant, vertex, points[i]);
        return sameHalfplane;
      });
    }

    // sort vertexes by angular coeffitient
    const leftVertexes = vertexes
      .filter((el) => el.x - points[i].x < 0)
      .sort((a, b) => (findSlope(points[i], a) > findSlope(points[i], b) ? 1 : -1));
    const rightVertexes = vertexes
      .filter((el) => el.x - points[i].x >= 0)
      .sort((a, b) => (findSlope(points[i], a) > findSlope(points[i], b) ? 1 : -1));
    vertexes = leftVertexes.concat(rightVertexes);

    if (checkIfFinite(vertexes, points[i])) {
      areas.push(Math.abs(findArea(vertexes)));
    } else {
      areas.push(-1);
    }
  }

  return areas;
}

const checkIfValidVertex = (vertex, site1, site2) => {
  const distance1 =
    (vertex.x - site1.x) * (vertex.x - site1.x) + (vertex.y - site1.y) * (vertex.y - site1.y);
  const distance2 =
    (vertex.x - site2.x) * (vertex.x - site2.x) + (vertex.y - site2.y) * (vertex.y - site2.y);

  return epsEqu(distance1, distance2) || distance1 < distance2;
};

const checkHalfplane = (l, vertex, site) => {
  if (l.slope === Infinity) {
    return (vertex.x >= l.p1.x && site.x >= l.p1.x) || (vertex.x <= l.p1.x && site.x <= l.p1.x);
  }

  const lineEquation = findLineEquation(l.p1, l.p2);

  const vertexHalfplane = lineEquation.a * vertex.x + vertex.y + lineEquation.c;

  if (epsEqu(vertexHalfplane, 0)) {
    return true;
  }

  const siteHalfplane = lineEquation.a * site.x + site.y + lineEquation.c;

  return vertexHalfplane * siteHalfplane >= 0;
};

const epsEqu = (a, b) => {
  if (Math.abs(a) < 2.65661287307739e-10 && Math.abs(b) < 2.65661287307739e-10) {
    return true;
  }

  if (Math.abs(a) >= 900719925474099 && Math.abs(b) >= 900719925474099) {
    return true;
  }

  if ((a === Infinity || b === Infinity) && (a !== Infinity || b !== Infinity)) {
    return false;
  }

  return Math.abs(a - b) <= (Number.EPSILON * 1000 * Math.max(Math.abs(a), Math.abs(b))) / 8;
};

const findLineEquation = (p1, p2) => {
  //ax + y + c = 0
  const point1 = p2.x > p1.x ? p1 : p2;
  const point2 = p2.x > p1.x ? p2 : p1;
  const dy = point2.y - point1.y;
  const dx = point2.x - point1.x;
  const a = -dy / dx;
  const c = (point1.x * dy) / dx - point1.y;

  return { a, c };
};

const findMidpoint = (p1, p2) => {
  const x = (p1.x + p2.x) / 2;
  const y = (p1.y + p2.y) / 2;

  return {
    x: epsEqu(x, 0) ? 0 : x,
    y: epsEqu(y, 0) ? 0 : y,
  };
};

const findSecantSlope = (p1, p2) => {
  const slope = findSlope(p1, p2);

  if (epsEqu(slope, 0)) {
    return Number.POSITIVE_INFINITY;
  }

  if (epsEqu(slope, Number.POSITIVE_INFINITY)) {
    return 0;
  }

  return -(1 / slope);
};

const findSlope = (p1, p2) => {
  if (epsEqu(p1.x, p2.x)) {
    return Number.POSITIVE_INFINITY;
  }

  if (epsEqu(p2.y, p1.y)) {
    return 0;
  }

  return (p2.y - p1.y) / (p2.x - p1.x);
};

const findSecondPoint = (m, slope) => {
  if (slope == Number.POSITIVE_INFINITY) {
    return {
      x: m.x,
      y: m.y + 1,
    };
  }

  if (epsEqu(m.x, 0)) {
    return {
      x: 1,
      y: m.y + slope,
    };
  }

  const y = m.y - m.x * slope;

  return {
    x: 0,
    y: epsEqu(y, 0) ? 0 : y,
  };
};

const findIntersectionPoint = (l1, l2) => {
  if (l1.slope === l2.slope) {
    return false;
  }

  if (epsEqu(l1.slope, l2.slope)) {
    return false;
  }

  if (l1.slope === Infinity) {
    const y = l2.p1.y + (l1.p1.x - l2.p1.x) * l2.slope;
    return {
      x: l1.p1.x,
      y: epsEqu(y, 0) ? 0 : y,
    };
  }

  if (l2.slope === Infinity) {
    const y = l1.p1.y + (l2.p1.x - l1.p1.x) * l1.slope;

    return {
      x: l2.p1.x,
      y: epsEqu(y, 0) ? 0 : y,
    };
  }

  const b1 = l1.p1.y - l1.slope * l1.p1.x;
  const b2 = l2.p1.y - l2.slope * l2.p1.x;
  const x = (b2 - b1) / (l1.slope - l2.slope);
  const y = l1.slope * x + b1;

  return {
    x: epsEqu(x, 0) ? 0 : x,
    y: epsEqu(y, 0) ? 0 : y,
  };
};

const checkIfFinite = (vertexes, site) => {
  let isFinite = true;
  if (vertexes.length > 2) {
    for (let v1 = 0; v1 < vertexes.length; v1++) {
      let v2 = v1 + 1;
      if (v1 == vertexes.length - 1) {
        v2 = 0;
      }
      const edge = {
        p1: {
          x: vertexes[v1].x,
          y: vertexes[v1].y,
        },
        p2: {
          x: vertexes[v2].x,
          y: vertexes[v2].y,
        },
        slope: findSlope(vertexes[v1], vertexes[v2]),
      };
      const otherVertex = vertexes.find((_, i) => i !== v1 && i !== v2);

      if (!checkHalfplane(edge, otherVertex, site)) {
        isFinite = false;
        break;
      }
    }
  } else {
    isFinite = false;
  }

  return isFinite;
};

const findArea = (vertexes) => {
  let doubleArea = 0;
  for (let i = 0; i < vertexes.length; i++) {
    if (i === vertexes.length - 1) {
      doubleArea += vertexes[i].x * vertexes[0].y - vertexes[i].y * vertexes[0].x;
    } else {
      doubleArea += vertexes[i].x * vertexes[i + 1].y - vertexes[i].y * vertexes[i + 1].x;
    }
  }
  return doubleArea / 2;
};